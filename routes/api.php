<?php

use App\Http\Controllers\AuthController;;
use App\Http\Controllers\MiscController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */
Route::group(['prefix' => '/v1'], function () {

    Route::group(['prefix' => '/auth'], function () {
        Route::group(['middleware' => ['throttle:authentication']], function () {
            Route::post('login', [AuthController::class, 'login']);
            Route::post('login/social-check', [AuthController::class, 'socialCheck']);
            Route::post('register', [AuthController::class, 'register']);
            Route::post('register/social', [AuthController::class, 'social']);
        });
    });

    Route::get('api-version', [MiscController::class, 'apiVersion']);

    Route::group(['prefix' => '/private', 'middleware' => ['auth:sanctum', 'abilities:api_access']], function () {
        Route::group(['prefix' => '/miscs'], function () {
            Route::post('list-province', [MiscController::class, 'indexProvince']);
            Route::post('list-city', [MiscController::class, 'indexCity']);
        });
    });
  

});
