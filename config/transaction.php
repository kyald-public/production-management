<?php


return [
    'status' => [
        'EXPIRED' => -2,
        'CANCEL' => -1,
        'WAITING_FOR_PAYMENT' => 0,
        'OPENED' => 1,
        'COMPLETE' => 2,
        'DEVICE_ON' => 3,
        'DEVICE_OFF' => 4,
        'STOP' => 99,
    ],
    'table_availability' => [
        'AVAILABLE' => 1,
        'NOT_AVAILABLE' => 0,
    ],
    'minutes_expired_payment' => 5,
    'device_status' => [
        'ON' => 'online',
        'OFF' => 'offline',
    ],
    'device_state' => [
        'IDLE' => 'idle',
        'ON_OPENED' => 'open',
    ],
    'mqtt_topic_status' => [
        'ON_OPENED' => 0,
        'WAITING_FOR_ON' => 1,
        'WAITING_FOR_OFF' => 2,
    ],
    'mqtt_client_id' => [
        'FIRST' => '001',
        'PUBLISH' => '002',
        'SIMULATOR_PUBLISH' => '003',
        'SUBSCRIBE' => '004',
    ],
    'device_responses' => [
        'STATUS' => 'status',
        'STATE' => 'state',
        'STARTED_AT' => 'startedAt',
    ],
];