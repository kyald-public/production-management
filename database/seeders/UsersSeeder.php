<?php

namespace Database\Seeders;

use App\Models\City;
use App\Models\Merchant;
use App\Models\Outlet;
use App\Models\PosPaymentMethod;
use App\Models\PosPaymentMethodStore;
use App\Models\Province;
use App\Models\Store;
use App\Models\User;
use App\Models\Admin;
use App\Models\UserMerchant;
use App\Models\UserOutlet;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use File;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $admin = Admin::create([
            'username' => 'admin',
            'name' => 'Sample Name',
            'email' => 'admin@email.com',
            'phone' => '+628138239' . rand(1000, 9999),
            'email_verified_at' => now(),
            'uuid' => Str::uuid(),
            'password' => bcrypt('admin'), // password
            'pin' => bcrypt('123456'), // password
            'remember_token' => Str::random(10),
        ]);

        $jsonProvinces = File::get(database_path('json/provinces.json'));
        $dataProvinces = json_decode($jsonProvinces);
        $dataProvinces = collect($dataProvinces);

        foreach ($dataProvinces as $d) {
            $d = collect($d)->toArray();
            $p = new Province();
            // $p->fill($d);
            $p->province_id = $d['province_id'];
            $p->uuid = Str::uuid();
            $p->name = $d['province_name'];
            $p->save();
        }

        $jsonCities = File::get(database_path('json/cities.json'));
        $dataCities = json_decode($jsonCities);
        $dataCities = collect($dataCities);

        foreach ($dataCities as $d) {
            $d = collect($d)->toArray();
            $p = new City();
            // $p->fill($d);
            $p->uuid =  Str::uuid();
            $p->name = $d['city_name'];
            $p->province_id = $d['province_id'];
            $p->save();
        }

        if (getEnv('APP_ENV') != 'production') {

            $user = User::create([
                'username' => 'admin',
                'name' => 'Sample Name',
                'email' => 'admin@email.com',
                'phone' => '+628138239' . rand(1000, 9999),
                'email_verified_at' => now(),
                'uuid' => Str::uuid(),
                'has_created_password' => 1,
                'password' => bcrypt('admin'), // password
                'pin' => bcrypt('123456'), // password
                'remember_token' => Str::random(10),
            ]);

            // Inventory Account Test
            $inventoryUser = User::create([
                'username' => 'Inventory 1',
                'name' => 'Inventory 1',
                'email' => 'inventory1@email.com',
                'phone' => '+628138239' . rand(1000, 9999),
                'email_verified_at' => now(),
                'uuid' => Str::uuid(),
                'has_created_password' => 1,
                'password' => bcrypt('admin'), // password
                'pin' => bcrypt('123456'), // password
                'remember_token' => Str::random(10),
            ]);

            // Supervisor Account Test
            $spv = User::create([
                'username' => 'Supervisor 1',
                'name' => 'Supervisor 1',
                'email' => 'spv1@email.com',
                'phone' => '+628138239' . rand(1000, 9999),
                'email_verified_at' => now(),
                'uuid' => Str::uuid(),
                'has_created_password' => 1,
                'password' => bcrypt('admin'), // password
                'pin' => bcrypt('123456'), // password
                'remember_token' => Str::random(10),
            ]);
        }

        // User Merchant Pivot Table
    }
}
