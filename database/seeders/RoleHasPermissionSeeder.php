<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleHasPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = array();

        for ($i = 1; $i <= 10; $i++) {
            array_push($data,
                [
                    'permission_id' => $i,
                    'role_id' => 1,
                ]
            );
        }

        for ($i = 1; $i <= 10; $i++) {
            array_push($data,
                [
                    'permission_id' => $i,
                    'role_id' => 3,
                ]
            );
        }

        DB::table('role_has_permissions')->insert($data);
    }

}
