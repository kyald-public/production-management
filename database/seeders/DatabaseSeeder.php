<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            RoleAndPermissionSeeder::class,
            //start seeder permission
            PermissionUserSeeder::class,                 //1
            PermissionModelHasPermissionSeeder::class,   //3
            PermissionModelHasRoleSeeder::class,         //4
            RoleHasPermissionSeeder::class,
            ModelHasPermissionSeeder::class,
            ModelHasRoleSeeder::class,
            //seeder permission end
            UsersSeeder::class,

        ]);

    }
}
