<?php

namespace App\Repositories;

use App\Http\Controllers\BaseController;
use App\Http\Resources\MiscResources\MiscShowBasicResource;
use App\Interfaces\MiscInterface;
use App\Models\City;
use App\Models\Province;
use App\Models\UnitMeasure;
use App\PipelineFilters\UserPipeline\GetByWord;
use App\PipelineFilters\UserPipeline\UseSort;
use App\PipelineFilters\MiscPipeline\MiscQueryPipeline;
use Illuminate\Pipeline\Pipeline;


class MiscRepository extends BaseController implements MiscInterface
{

    public function indexCity($request, $getOnlyColumn)
    {

        try {
            $getData = City::where('province_id', $request['province_id'])->get();

            dd(json_encode($getData));

            $message = 'show misc list with paginate success';

            return $this->handleQueryArrayResponse($getData, $message);

        } catch (\Exception $e) {

            return $this->handleQueryErrorArrayResponse($e->getMessage(), 'error when get users');
        }
    }

    public function indexProvince($request, $getOnlyColumn)
    {

        try {
            $getData = app(Pipeline::class)
                ->send(Province::query())
                ->through([
                    GetByWord::class,
                    // GetByKey::class,
                    UseSort::class,
                ])
                ->thenReturn()
                ->with('roles.permissions')
                ->select($getOnlyColumn);

            if (request()->get('paginate') == true) {
                $outputData = $getData->paginate(request()->get('per_page'), $getOnlyColumn, 'page', request()->get('currentPage'));
                $getCollection = $outputData->getCollection();
            } else {

                $getCollection = $getData->limit(250)->get();
            }

            $itemsTransformed = $getCollection
                ->map(function ($item) {
                    return $this->miscResourceFormat(request()->get('misc_type'), $item);
                });

            // if (count($getCollection) > 1 || request()->get('paginate') == true) {

            //     $itemsTransformed = $itemsTransformed->toArray();
            // } else {
            //     $itemsTransformed = $itemsTransformed->first();
            // }
            $itemsTransformed = $itemsTransformed->toArray();

            if (request()->get('paginate') == true) {
                $outputData = new \Illuminate\Pagination\LengthAwarePaginator(
                    $itemsTransformed,
                    $outputData->total(),
                    $outputData->perPage(),
                    $outputData->currentPage(), [
                        'path' => \Request::url(),
                        'query' => request()->all(),
                    ]
                );

                $message = 'show misc list with paginate success';
            } else {

                $outputData = $itemsTransformed;
                if (count($getCollection) > 1) {
                    $message = 'show users data success without pagination max 250 data';
                } else {
                    $message = 'show users data success';
                }

            }

            return $this->handleQueryArrayResponse($outputData, $message);

        } catch (\Exception $e) {

            return $this->handleQueryErrorArrayResponse($e->getMessage(), 'error when get users');
        }
    }

    public function indexUom($request, $getOnlyColumn)
    {

        try {
            $getData = app(Pipeline::class)
                ->send(UnitMeasure::query())
                ->through([
                    GetByWord::class,
                    // GetByKey::class,
                    UseSort::class,
                ])
                ->thenReturn()
                ->with('roles.permissions')
                ->select($getOnlyColumn);

            if (request()->get('paginate') == true) {
                $outputData = $getData->paginate(request()->get('per_page'), $getOnlyColumn, 'page', request()->get('currentPage'));
                $getCollection = $outputData->getCollection();
            } else {

                $getCollection = $getData->limit(250)->get();
            }

            $itemsTransformed = $getCollection
                ->map(function ($item) {
                    return $this->miscResourceFormat(request()->get('misc_type'), $item);
                });

            // if (count($getCollection) > 1 || request()->get('paginate') == true) {

            //     $itemsTransformed = $itemsTransformed->toArray();
            // } else {
            //     $itemsTransformed = $itemsTransformed->first();
            // }
            $itemsTransformed = $itemsTransformed->toArray();

            if (request()->get('paginate') == true) {
                $outputData = new \Illuminate\Pagination\LengthAwarePaginator(
                    $itemsTransformed,
                    $outputData->total(),
                    $outputData->perPage(),
                    $outputData->currentPage(), [
                        'path' => \Request::url(),
                        'query' => request()->all(),
                    ]
                );

                $message = 'show misc list with paginate success';
            } else {

                $outputData = $itemsTransformed;
                if (count($getCollection) > 1) {
                    $message = 'show users data success without pagination max 250 data';
                } else {
                    $message = 'show users data success';
                }

            }

            return $this->handleQueryArrayResponse($outputData, $message);

        } catch (\Exception $e) {

            return $this->handleQueryErrorArrayResponse($e->getMessage(), 'error when get users');
        }
    }

    public function indexRole($request, $getOnlyColumn)
    {

        try {
            $getData = \Spatie\Permission\Models\Role::all();

            $getData->shift();

            $message = 'show misc list with paginate success';

            return $this->handleQueryArrayResponse($getData, $message);

        } catch (\Exception $e) {

            return $this->handleQueryErrorArrayResponse($e->getMessage(), 'error when get users');
        }
    }

    private function miscResourceFormat($formatType, $data)
    {

        // if ($formatType == 'showAll') {
        //     return new MiscShowAllWebResource([
        //         'data' => $data,
        //         'status' => true,
        //     ]);
        // } else if ($formatType == 'showBasic') {

        //     return new MiscShowBasicResource([
        //         'data' => $data,
        //         'status' => true,
        //     ]);
        // }

        return new MiscShowBasicResource([
            'data' => $data,
            'status' => true,
        ]);
    }

}
