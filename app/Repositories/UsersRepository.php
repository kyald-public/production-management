<?php

namespace App\Repositories;

use App\Http\Controllers\BaseController;
use App\Http\Resources\UsersResources\UsersShowDetailResource;
use App\Http\Resources\UsersResources\UserShowAllWebResource;
use App\Http\Resources\UsersResources\UsersShowBasicResource;
use App\Interfaces\UserInterface;
use App\Models\User;
use App\Models\UserMerchant;
use App\Models\UserOutlet;
use App\PipelineFilters\UserPipeline\UserQueryPipeline;
use App\PipelineFilters\UserPipeline\UserProfilePipeline;
use Auth;
use Illuminate\Pipeline\Pipeline;
use Illuminate\Support\Facades\Hash;
use Str;
use App\PipelineFilters\UserPipeline\GetByWord;
use App\PipelineFilters\UserPipeline\GetByKey;
use App\PipelineFilters\UserPipeline\GetBySort;
use App\PipelineFilters\UserPipeline\UseSort;


class UsersRepository extends BaseController implements UserInterface
{

    public function index($request, $getOnlyColumn)
    {

        try {
            $getData = app(Pipeline::class)
                ->send($request)
                ->through([
                    UserQueryPipeline::class,
                ])
                ->thenReturn()
                ->with('roles.permissions')
                ->select($getOnlyColumn);

            if (request()->get('paginate') == true) {
                $outputData = $getData->paginate(request()->get('per_page'), $getOnlyColumn, 'page', request()->get('currentPage'));
                $getCollection = $outputData->getCollection();
            } else {

                $getCollection = $getData->limit(250)->get();
            }

            $itemsTransformed = $getCollection
                ->map(function ($item) {
                    return $this->userResourceFormat(request()->get('collection_type'), $item);
                });

            if (request()->get('paginate') == true) {
                $outputData = new \Illuminate\Pagination\LengthAwarePaginator(
                    $itemsTransformed,
                    $outputData->total(),
                    $outputData->perPage(),
                    $outputData->currentPage(), [
                        'path' => \Request::url(),
                        'query' => request()->all(),
                    ]
                );

                $message = 'show user list with paginate success';
            } else {

                $outputData = $itemsTransformed;
                if (count($getCollection) > 1) {
                    $message = 'show users data success without pagination max 250 data';
                } else {
                    $message = 'show users data success';
                }

            }

            return $this->handleQueryArrayResponse($outputData, $message);

        } catch (\Exception $e) {

            return $this->handleQueryErrorArrayResponse($e->getMessage(), 'error when get users');
        }
    }

    public function show($request, $getOnlyColumn)
    {

        try {
            // $getData = app(Pipeline::class)
            //     ->send(User::query())
            //     ->through([
            //         GetByWord::class,
            //         GetByKey::class,
            //         UseSort::class,
            //     ])
            //     ->thenReturn()
            //     ->with('roles.permissions')
            //     ->select($getOnlyColumn);
            $getData = app(Pipeline::class)
                ->send($request)
                ->through([
                    UserQueryPipeline::class,
                ])
                ->thenReturn()
                ->with('roles.permissions')
                ->select($getOnlyColumn);

            if (request()->get('paginate') == true) {
                $outputData = $getData->paginate(request()->get('per_page'), $getOnlyColumn, 'page', request()->get('currentPage'));
                $getCollection = $outputData->getCollection();
            } else {

                $getCollection = $getData->limit(250)->get();
            }

            $itemsTransformed = $getCollection
                ->map(function ($item) {

                    return $this->userResourceFormat('showDetail', $item);

                });

            $itemsTransformed = $itemsTransformed->first();

            if (request()->get('paginate') == true) {
                $outputData = new \Illuminate\Pagination\LengthAwarePaginator(
                    $itemsTransformed,
                    $outputData->total(),
                    $outputData->perPage(),
                    $outputData->currentPage(), [
                        'path' => \Request::url(),
                        'query' => request()->all(),
                    ]
                );

                $message = 'show users with paginate success';
            } else {

                $outputData = $itemsTransformed;
                if (count($getCollection) > 1) {
                    $message = 'show users data success without pagination max 250 data';
                } else {
                    $message = 'show users data success';
                }

            }

            return $this->handleQueryArrayResponse($outputData, $message);

        } catch (\Exception $e) {

            return $this->handleQueryErrorArrayResponse($e->getMessage(), 'error when get users');
        }
    }

    public function showProfile($request, $getOnlyColumn)
    {

        try {
            $getData = app(Pipeline::class)
                ->send($request)
                ->through([
                    UserProfilePipeline::class,
                ])
                ->thenReturn()
                ->with('roles.permissions')
                ->select($getOnlyColumn);

            if (request()->get('paginate') == true) {
                $outputData = $getData->paginate(request()->get('per_page'), $getOnlyColumn, 'page', request()->get('currentPage'));
                $getCollection = $outputData->getCollection();
            } else {

                $getCollection = $getData->limit(250)->get();
            }

            $itemsTransformed = $getCollection
                ->map(function ($item) {

                    return $this->userResourceFormat('showAll', $item);

                });

            $itemsTransformed = $itemsTransformed->first();

            if (request()->get('paginate') == true) {
                $outputData = new \Illuminate\Pagination\LengthAwarePaginator(
                    $itemsTransformed,
                    $outputData->total(),
                    $outputData->perPage(),
                    $outputData->currentPage(), [
                        'path' => \Request::url(),
                        'query' => request()->all(),
                    ]
                );

                $message = 'show users with paginate success';
            } else {

                $outputData = $itemsTransformed;
                if (count($getCollection) > 1) {
                    $message = 'show users data success without pagination max 250 data';
                } else {
                    $message = 'show users data success';
                }

            }

            return $this->handleQueryArrayResponse($outputData, $message);

        } catch (\Exception $e) {

            return $this->handleQueryErrorArrayResponse($e->getMessage(), 'error when get users');
        }
    }

    public function store($data, $returnCollection = 'showAll')
    {

        $user = Auth::user();

        try {
            $users = new User();
            $users->name = $data['name'];
            $users->username = $data['username'];
            $users->email = $data['email'];
            $users->phone = $data['phone'];
            $users->password = Hash::make($data['password']);
            $users->pin = Hash::make($data['pin']);
            $users->has_created_password = 1;
            // $users->email_verified_at = now();
            $users->save();

            if ($data['role'] !== 'admin') {
                $users->assignRole($data['role']);
            }

            $usersMerchant = new UserMerchant();
            $usersMerchant->merchant_id = $user->merchant->merchant_id;
            $usersMerchant->user_id = $users->id;
            $usersMerchant->save();

            foreach ($data['outlets'] as $key => $outlet) {
                $data['outlets'][$key]['user_id'] = $users->id;
                $data['outlets'][$key]['merchant_id'] = Auth::user()->merchant->merchant_id;

                $userOutlet = UserOutlet::create($data['outlets'][$key]);
            }

            if ($users) {

                $getAfterInsertData = $this->userResourceFormat($returnCollection, $users);

                return $this->handleQueryArrayResponse($getAfterInsertData, 'insert users Success');

            } else {

                return $this->handleQueryErrorArrayResponse($users, 'insert users fail');
            }

        } catch (\Exception $e) {

            return $this->handleQueryErrorArrayResponse($e->getMessage(), $e->getMessage());
        }
    }
    public function destroy($id)
    {
        try {
            $remove = User::where('id', $id)->delete();

            if ($remove) {
                return $this->handleQueryArrayResponse($remove, 'destroy users success');
            } else {
                return $this->handleQueryErrorArrayResponse($remove, 'destroy users fail');

            }

        } catch (\Exception $e) {
            return $this->handleQueryErrorArrayResponse($e->getMessage(), 'error when destroy user');
        }
    }
    public function update($data, $returnCollection = 'showAll')
    {

        try {
            $users = User::where(['id' => $data['id']])->first();


            if ($users) {

                $users->name = $data['name'];
                $users->username = $data['username'];
                $users->email = $data['email'];
                $users->phone = $data['phone'];

                if(isset($data['password'])){
                    $users->password = Hash::make($data['password']);
                }
                if(isset($data['pin'])){
                    $users->pin = Hash::make($data['pin']);
                }

                $users->save();

                // $users->update($data);

                // Add Outlets if available
                foreach ($data['outlets'] as $key => $outlet) {

                    if(!$data['outlets'][$key]['user_outlet_id']){
                        $data['outlets'][$key]['uuid'] = Str::uuid();
                    }
                    $data['outlets'][$key]['user_id'] = $users->id;
                    $data['outlets'][$key]['merchant_id'] = Auth::user()->merchant->merchant_id;
                }

                $userOutlets = $users->outlets()->sync($data['outlets']);

                return $this->handleQueryArrayResponse($users, 'update users success');

            } else {
                return $this->handleQueryErrorArrayResponse($users, 'updates fail - users not found');
            }
        } catch (\Exception $e) {
            // dd($e->getMessage());
            return $this->handleQueryErrorArrayResponse($e->getMessage(), 'error when update user');
        }
    }

    public function refreshtoken($data, $returnCollection = 'showAll')
    {
        try {
            $users = Auth::user();

            if ($users) {

                $users->device_id = $data['device_id'];

                $users->save();

                return $this->handleQueryArrayResponse($users, 'update users success');

            } else {
                return $this->handleQueryErrorArrayResponse($users, 'updates fail - users not found');
            }
        } catch (\Exception $e) {
            // dd($e->getMessage());
            return $this->handleQueryErrorArrayResponse($e->getMessage(), 'error when update user');
        }
    }

    private function userResourceFormat($formatType, $data)
    {

        if ($formatType == 'showAll') {

            return new UserShowAllWebResource([
                'data' => $data,
                'status' => true,
            ]);
        } else if ($formatType == 'showBasic') {

            return new UsersShowBasicResource([
                'data' => $data,
                'status' => true,
            ]);

        }else if ($formatType == 'showDetail') {

            return new UsersShowDetailResource([
                'data' => $data,
                'status' => true,
            ]);

        }
    }

}
