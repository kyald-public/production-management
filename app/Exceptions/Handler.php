<?php

namespace App\Exceptions;

use App\Http\Controllers\BaseController;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;
use \Spatie\Permission\Exceptions\UnauthorizedException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */

    protected function invalidJson($request, ValidationException $exception)
    {
        $response = new BaseController;
        $error = $this->transformErrors($exception);
        return $response->handleError(
            $error,
            $exception->getMessage(),
            $request->all(),
            str_replace('/', '.', $request->path()),
            $exception->status
        );
    }

// transform the error messages,
    private function transformErrors(ValidationException $exception)
    {

        $errors = [];

        foreach ($exception->errors() as $field => $message) {
            $errors[] = [
                'field' => $field,
                'message' => $message[0],
            ];
        }
        return $errors;
    }

    // protected function unauthenticated($request, AuthenticationException $exception)
    // {
    //     if ($request->expectsJson()) {
    //         $json = [
    //             'isAuth' => false,
    //             'message' => $exception->getMessage(),
    //         ];
    //         return response()
    //             ->json($json, 401);
    //     }
    //     $guard = array_get($exception->guards(), 0);
    //     switch ($guard) {
    //         default:
    //             $login = 'login';
    //             break;
    //     }
    //     return redirect()->guest(route($login));
    // }

    public function register()
    {

        $this->reportable(function (Throwable $e) {
            //
        });

        $this->renderable(function (UnauthorizedException $e, $request) {
            $response = new BaseController;

            $errorMessage = array([
                'field' => 'spatie authorization',
                'message' => 'you dont have this access',
            ]);

            return $response->handleError(
                $errorMessage,
                'access unathorized',
                $request->all(),
                str_replace('/', '.', $request->path()),
                403,
                'warning'
            );
        });

        $this->renderable(function (AccessDeniedHttpException $e, $request) {

            $response = new BaseController;

            $errorMessage = array([
                'field' => 'authorization',
                'message' => 'you dont have this access',
            ]);

            return $response->handleError(
                $errorMessage,
                'access unathorized',
                $request->all(),
                str_replace('/', '.', $request->path()),
                403,
                'warning'
            );
        });

        $this->renderable(function (QueryException $e, $request) {

            $response = new BaseController;

            $errorMessage = array([
                'field' => 'process error',
                'message' => 'please contact administrator for assistance',
            ]);

            return $response->handleError(
                $errorMessage,
                $e,
                $request->all(),
                str_replace('/', '.', $request->path()),
                500,
                'error'
            );
        });

        $this->renderable(function (HttpException $e, $request) {

            $data = Auth::user($request->header('Authorization'));

            if ($data) {

                $errorMessage = array([
                    'field' => 'account_verification',
                    'message' => 'forbidden access, verify users access only',
                ]);
                if (!$request->user()->hasVerifiedEmail()) {
                    $response = new BaseController;
                    return $response->handleError(
                        $errorMessage,
                        'user verification access only',
                        $request->all(),
                        str_replace('/', '.',
                            $request->path()),
                        403,
                        'warning'
                    );
                }
            }

        });

        $this->renderable(function (AuthenticationException $e, $request) {
            $response = new BaseController;

            $errorMessage = array([
                'field' => 'Unauthenticated',
                'message' => 'Please make sure you have access to the resources',
            ]);

            return $response->handleError(
                $errorMessage,
                'Unauthenticated',
                $request->all(),
                str_replace('/', '.', $request->path()),
                401,
                'error'
            );
        });

    }

}
