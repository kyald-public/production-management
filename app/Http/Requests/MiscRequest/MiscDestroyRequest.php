<?php

namespace App\Http\Requests\MiscRequest;

use App\Models\Misc;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class MiscDestroyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::user()->hasRole('admin')) {
            return true;
        }

        if (Auth::user()->can('miscs_destroy')) {
            return true;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'by_uuid' => 'required|exists:misc,uuid',
        ];
    }
    public function messages()
    {
        return [

            'by_uuid.required' => 'misc uuid perlu diisi',
            'by_uuid.exists' => 'misc uuid tidak tersedia',

        ];
    }
    protected function passedValidation()
    {

        //check if id registered as parent
        $checkdata = Misc::where('uuid', $this->by_uuid)->first();

        if (!$checkdata) {
            throw ValidationException::withMessages([
                'title' => ['destory fail,misc name not found'],
            ]);
        }
    }
}
