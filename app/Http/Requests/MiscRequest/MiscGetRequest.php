<?php

namespace App\Http\Requests\MiscRequest;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class MiscGetRequest extends FormRequest
{
/**
 * Determine if the user is authorized to make this request.
 *
 * @return bool
 */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    // protected function passedValidation()
    // {
        // $checkdata = Product::where('id', $this->by_id)->where('name', $this->product_name)->first();

        // if (!$checkdata) {
        //     throw ValidationException::withMessages([
        //         'title' => ['destory fail,product name not match'],
        //     ]);
        // }
    // }
}
