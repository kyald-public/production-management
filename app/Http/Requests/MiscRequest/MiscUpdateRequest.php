<?php

namespace App\Http\Requests\MiscRequest;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class MiscUpdateRequest extends FormRequest
{
/**
 * Determine if the user is authorized to make this request.
 *
 * @return bool
 */
    public function authorize()
    {

        if (Auth::user()->hasRole('admin')) {
            return true;
        }

        if (Auth::user()->can('miscs_update')) {
            return true;
        }

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'merchant_id' => 'nullable',
            'province_id' => 'nullable',
            'city_id' => 'nullable',
            'name' => 'nullable',
            'address' => 'nullable',
            'latitude' => 'nullable',
            'longitude' => 'nullable',
            'tax_charge' => 'nullable',
            'service_charge' => 'nullable',
            'status' => 'nullable',
        ];
    }

    public function messages()
    {
        return [

        ];
    }

    // protected function passedValidation()
    // {
        // $checkdata = Product::where('id', $this->by_id)->where('name', $this->product_name)->first();

        // if (!$checkdata) {
        //     throw ValidationException::withMessages([
        //         'title' => ['destory fail,product name not match'],
        //     ]);
        // }
    // }
}
