<?php

namespace App\Http\Requests\UserRequest;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class UserShowRequest  extends FormRequest
{
/**
 * Determine if the user is authorized to make this request.
 *
 * @return bool
 */
    public function authorize()
    {

        if (Auth::user()->hasRole('admin')) {
            return true;
        }

        if (Auth::user()->can('users_create')) {
            return true;
        }

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'merchant_id' => 'required',
            // 'province_id' => 'required',
            // 'city_id' => 'required',
            // 'name' => 'required',
            // 'address' => 'required',
            // 'latitude' => 'required',
            // 'longitude' => 'required',
            // 'tax_charge' => 'required',
            // 'service_charge' => 'required',
            // 'status' => 'required',
        ];
    }

    public function messages()
    {
        return [
            // 'province_id.required' => 'provinsi perlu diisi',
        ];
    }

    // protected function passedValidation()
    // {
        // $checkdata = Product::where('id', $this->by_id)->where('name', $this->product_name)->first();

        // if (!$checkdata) {
        //     throw ValidationException::withMessages([
        //         'title' => ['destory fail,product name not match'],
        //     ]);
        // }
    // }
}
