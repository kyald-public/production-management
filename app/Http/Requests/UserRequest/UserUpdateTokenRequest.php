<?php

namespace App\Http\Requests\UserRequest;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UserUpdateTokenRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'device_id' => 'required',
            // 'province_id' => 'required',
            // 'city_id' => 'required',
            // 'name' => 'required',
            // 'address' => 'required',
            // 'latitude' => 'required',
            // 'longitude' => 'required',
            // 'tax_charge' => 'required',
            // 'service_charge' => 'required',
            // 'status' => 'required',
        ];
    }

    protected function passedValidation()
    {

        // return $this->validate([
        //     'username' => 'required|string|exists:users,username',
        // ],
        //     [
        //         'username.required' => 'email wajib diisi',
        //         'username.exists' => 'email / password tidak sesuai',
        //     ]);

    }

    public function messages()
    {
        return [
            'device_id.required' => 'fcm token perlu diisi',
        ];
    }

    // protected function passedValidation()
    // {
    // $checkdata = Product::where('id', $this->by_id)->where('name', $this->product_name)->first();

    // if (!$checkdata) {
    //     throw ValidationException::withMessages([
    //         'title' => ['destory fail,product name not match'],
    //     ]);
    // }
    // }
}
