<?php

namespace App\Http\Requests\UserRequest;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class UserDestroyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::user()->hasRole('admin')) {
            return true;
        }

        if (Auth::user()->can('users_destroy')) {
            return true;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'by_uuid' => 'required|exists:user,uuid',
        ];
    }
    public function messages()
    {
        return [

            'by_uuid.required' => 'user uuid perlu diisi',
            'by_uuid.exists' => 'user uuid tidak tersedia',

        ];
    }
    protected function passedValidation()
    {

        //check if id registered as parent
        $checkdata = User::where('uuid', $this->by_uuid)->first();

        if (!$checkdata) {
            throw ValidationException::withMessages([
                'title' => ['destory fail,user name not found'],
            ]);
        }
    }
}
