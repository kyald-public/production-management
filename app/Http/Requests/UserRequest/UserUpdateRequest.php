<?php

namespace App\Http\Requests\UserRequest;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UserUpdateRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'email' => [
                'required',
                'max:255',
                Rule::unique('users')->ignore($this->request->get('id')),
            ],
            'username' => 'required|max:255|unique:users,username,'.$this->request->get('id').',users.id',
            'phone' => 'required|max:255|unique:users,phone,'.$this->request->get('id').',users.id',
            // 'merchant_id' => 'required',
            // 'province_id' => 'required',
            // 'city_id' => 'required',
            // 'name' => 'required',
            // 'address' => 'required',
            // 'latitude' => 'required',
            // 'longitude' => 'required',
            // 'tax_charge' => 'required',
            // 'service_charge' => 'required',
            // 'status' => 'required',
        ];
    }

    protected function passedValidation()
    {

        // return $this->validate([
        //     'username' => 'required|string|exists:users,username',
        // ],
        //     [
        //         'username.required' => 'email wajib diisi',
        //         'username.exists' => 'email / password tidak sesuai',
        //     ]);

    }

    public function messages()
    {
        return [
            'username.required' => 'provinsi perlu diisi',
        ];
    }

    // protected function passedValidation()
    // {
    // $checkdata = Product::where('id', $this->by_id)->where('name', $this->product_name)->first();

    // if (!$checkdata) {
    //     throw ValidationException::withMessages([
    //         'title' => ['destory fail,product name not match'],
    //     ]);
    // }
    // }
}
