<?php

namespace App\Http\Requests\AuthRequest;

use Illuminate\Foundation\Http\FormRequest;

class LoginUsersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function prepareForValidation()
    {
        if (is_numeric($this->username)) {

            $this->merge([
                'username' => $this->username,
                'isEmail' => false,
            ]);

        } else if (filter_var($this->username)) {

            $this->merge([
                'email' => $this->username,
                'isEmail' => true,
            ]);
        }
    }
    protected function passedValidation()
    {

        if ($this->isEmail == true) {
            return $this->validate([
                'email' => 'required|email|string|exists:users,email',
            ],
                [
                    'email.required' => 'email wajib diisi',
                    'email.email' => 'format email tidak sesuai',
                    'email.exists' => 'email / password tidak sesuai',
                ]);
        } else if ($this->isEmail == false) {

            $this->merge([
                'username' => $this->username,
                'country_phone' => $this->country_phone,
            ]);

            return $this->validate([
                'username' => 'required',
            ], [
                'username.required' => 'format username wajib diisi',
                'username.exists' => 'username / password tidak sesuai',
            ]);
        }

    }
    public function rules()
    {
        return [
            'username' => 'required',
            'password' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'username.required' => 'Email / username tidak dapat kosong',
            'username.email' => 'Format Email tidak sesuai',
            'password.required' => 'Password tidak dapat kosong',
            'password.required' => 'Password tidak dapat kosong',
        ];
    }
}
