<?php

namespace App\Http\Requests\AuthRequest;

use Illuminate\Foundation\Http\FormRequest;

class RegisterUsersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function prepareForValidation()
    {
        // if (is_numeric($this->username)) {

        //     $this->merge([
        //         'username' => $this->username,
        //         'isEmail' => false,
        //     ]);

        // } else if (filter_var($this->username)) {

        $this->merge([
            'email' => $this->username,
            'isEmail' => true,
        ]);
        // }
    }
    protected function passedValidation()
    {
        return $this->validate([
            'username' => 'required|unique:users|max:255|email',
        ], [
            'username.required' => 'format username wajib diisi',
            'username.exists' => 'username / password tidak sesuai',
        ]);

    }
    public function rules()
    {
        return [
            'username' => 'required',
            'password' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'username.required' => 'Email / username tidak dapat kosong',
            'username.email' => 'Format Email tidak sesuai',
            'password.required' => 'Password tidak dapat kosong',
        ];
    }
}
