<?php

namespace App\Http\Requests\AuthRequest;

use Illuminate\Foundation\Http\FormRequest;

class SocialUsersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'access_token' => $this->access_token,
            'provider' => $this->provider,
        ]);
    }
    protected function passedValidation()
    {

        $this->merge([
            'access_token' => $this->access_token,
            'provider' => $this->provider,
        ]);

        return $this->validate([
            'access_token' => 'required',
            'provider' => 'required',
        ], [
            'access_token.required' => 'format access_token wajib diisi',
            'provider.required' => 'format provider wajib diisi',
        ]);

    }
    public function rules()
    {
        return [
            'access_token' => 'required',
            'provider' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'access_token.required' => 'Access token tidak dapat kosong',
            'provider.required' => 'Provider tidak dapat kosong',
        ];
    }
}
