<?php

namespace App\Http\Requests\AuthRequest;

use Illuminate\Foundation\Http\FormRequest;

class LoginUsersPinRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'email' => $this->username,
            'isEmail' => true,
        ]);
    }
    protected function passedValidation()
    {

        if ($this->isEmail == true) {
            return $this->validate([
                'email' => 'required|email|string|exists:users,email',
            ],
                [
                    'email.required' => 'email wajib diisi',
                    'email.email' => 'format email tidak sesuai',
                    'email.exists' => 'email / password tidak sesuai',
                ]);
        }

    }
    public function rules()
    {
        return [
            'username' => 'required',
            'pin' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'username.required' => 'Email / username tidak dapat kosong',
            'username.email' => 'Format Email tidak sesuai',
            'pin.required' => 'Pin tidak dapat kosong',
            'pin.required' => 'Pin tidak dapat kosong',
        ];
    }
}
