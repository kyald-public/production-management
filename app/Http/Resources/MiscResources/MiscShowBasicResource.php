<?php

namespace App\Http\Resources\MiscResources;

use Illuminate\Http\Resources\Json\JsonResource;

class MiscShowBasicResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        if(request()->get('misc_type') == 'city'){
            return [
                'name'               => $this->resource['data']->name,
                'city_id'               => $this->resource['data']->city_id,
                'province_id'               => $this->resource['data']->province_id,
            ];
        } else if (request()->get('misc_type') == 'province'){
            return [
                'name'               => $this->resource['data']->name,
                'province_id'               => $this->resource['data']->province_id,
            ];
        } else if (request()->get('misc_type') == 'uom'){
            return [
                'name'               => $this->resource['data']->name
            ];
        }
    }

}
