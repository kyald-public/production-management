<?php

namespace App\Http\Middleware;

use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use \Carbon\Carbon;

class AuthApiKey
{

    protected $except = [
    ];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, \Closure $next, $guard = null)
    {

        // dd($request);

        foreach ($this->except as $excluded_route) {
            if (str_contains($request->path(), $excluded_route)) {
                \Log::debug("Skipping $excluded_route from auth check...");
                return  $next($request);
            }
        }

        if(!$request->headers->has('x-api-key')) {
            if(!$request->input('x-api-key')) {
                if ($request->expectsJson()) {
                    throw new UnauthorizedHttpException('Barrier', 'x-api-key required');
                } else {
                    return route('auth.login');
                }

            }
        } else {

            if($request->headers->get('x-api-key') == env("X_API_KEY")){
                if (in_array($request->method(), ['GET', 'DELETE']) ) {
                    $request->query->add(['x-api-key' => $request->headers->get('x-api-key')]);
                } else {
                    $request->request->add(['x-api-key' => $request->headers->get('x-api-key')]);
                }
            } else {
                if ($request->expectsJson()) {

                    $res = [
                        'code' => 401,
                        'timestamp' => Carbon::now()->toDateTimeString(),
                        'message' => 'Unauthorized',

                    ];
                    if (!empty($result)) {
                        $res['data'] = $result;
                    }

                    // $this->logProcess($logType, $res, 'handleError', $requestNo);

                    return response()->json($res, 401);

                } else {
                    return route('auth.login');
                }
            }
        }

        return $next($request);
    }
}
