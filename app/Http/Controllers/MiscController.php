<?php

namespace App\Http\Controllers;

use App\Http\Requests\MiscRequest\MiscCreateRequest;
use App\Http\Requests\MiscRequest\MiscUpdateRequest;
use App\Http\Requests\MiscRequest\MiscDestroyRequest;
use App\Http\Requests\MiscRequest\MiscShowRequest;
use App\Http\Requests\MiscRequest\MiscGetRequest;

use App\Interfaces\MiscInterface;
use App\Services\MiscService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MiscController extends BaseController
{

    private $MiscService;
    private $miscInterface;

    public function __construct(
        MiscService $MiscService,
        MiscInterface $miscInterface
    ) {
        $this->MiscService = $MiscService;
        $this->miscInterface = $miscInterface;
    }

    public function apiVersion(MiscGetRequest $request)
    {
        
        $previousApiVersion = getEnv('PREVIOUS_API_VERSION');
        $apiVersion = getEnv('API_VERSION');
        $previousAppVersion = getEnv('PREVIOUS_APP_VERSION');
        $appVersion = getEnv('APP_VERSION');

        $data = [
            'app_version' => $appVersion,
            'api_version' => $apiVersion
        ];

        return $this->handleResponse($data, 'show version success', $request->all(), str_replace('/', '.', $request->path()), 200);
    }


    public function indexCity(MiscShowRequest $request)
    {
        request()->request->add([
            'by_id' => auth('sanctum')->user()->id,
            'collection_type' => 'showBasic',
            'misc_type' => 'city'
        ]);

        $columnRequest = array('*');
        $getMiscList = $this->miscInterface->indexCity(request()->all(), $columnRequest);

        if ($getMiscList['queryStatus']) {

            return $this->handleResponse($getMiscList['queryResponse'], 'show misc list success', $request->all(), str_replace('/', '.', $request->path()), 200);
        }

        return $getMiscList;

    }


    public function indexProvince(MiscShowRequest $request)
    {
        request()->request->add([
            'by_id' => auth('sanctum')->user()->id,
            'collection_type' => 'showBasic',
            'misc_type' => 'province'
        ]);

        $columnRequest = array('*');
        $getMiscList = $this->miscInterface->indexProvince($request->all(), $columnRequest);

        if ($getMiscList['queryStatus']) {

            return $this->handleResponse($getMiscList['queryResponse'], 'show misc list success', $request->all(), str_replace('/', '.', $request->path()), 200);
        }

        return $getMiscList;

    }


    public function indexUom(MiscShowRequest $request)
    {
        request()->request->add([
            'by_id' => auth('sanctum')->user()->id,
            'collection_type' => 'showBasic',
            'misc_type' => 'uom'
        ]);

        $columnRequest = array('*');
        $getMiscList = $this->miscInterface->indexUom($request->all(), $columnRequest);

        if ($getMiscList['queryStatus']) {

            return $this->handleResponse($getMiscList['queryResponse'], 'show misc list success', $request->all(), str_replace('/', '.', $request->path()), 200);
        }

        return $getMiscList;

    }

    public function indexRole(MiscShowRequest $request)
    {

        request()->request->add([
            'by_id' => auth('sanctum')->user()->id,
            'collection_type' => 'showBasic',
            'misc_type' => 'role'
        ]);

        $columnRequest = array('*');
        $getMiscList = $this->miscInterface->indexRole($request->all(), $columnRequest);

        if ($getMiscList['queryStatus']) {

            return $this->handleResponse($getMiscList['queryResponse'], 'show misc list success', $request->all(), str_replace('/', '.', $request->path()), 200);
        }

        return $getMiscList;

    }

}
