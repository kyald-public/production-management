<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Midtrans\Notification;
use Midtrans\Config;
use Illuminate\Support\Facades\Log;

class MidtransController extends Controller
{

    public function __construct()
    {
    }

    public function callback(Request $request)
    {
        Config::$serverKey = config('services.midtrans.server_key');
        Config::$isProduction = config('services.midtrans.is_production');

        $notification = new Notification();

        $splices = explode('-', $notification->order_id);
        $playTransactionId = $splices[0];

        // Log::channel('midtrans')->info('Midtrans Notification Received', [
        //     'raw' => $request->getContent(),
        //     'parsed' => [
        //         'order_id' => $notification->order_id,
        //         'transaction_id' => $notification->transaction_id,
        //     ],
        // ]);

        $playTransaction = PlayTransaction::find($playTransactionId);
        // config(['app.locale' => $playTransaction->user->lang]);
        
        if ($notification->transaction_status == 'capture' || $notification->transaction_status == 'settlement') {

            $this->transactionrepository->updateTransaction('success');
            $statusText = 'success';
            $status = 1;
        } elseif ($notification->transaction_status == 'pending') {
            // Payment is pending
            // Handle accordingly

            $statusText = 'pending';
            $status = 0;
        } else {
            // Payment failed or other status
            // Handle accordingly
            $statusText = 'failed';
            $status = -1;
        }

        if ($notification->transaction_status != 'pending') {
            PaymentLog::create([
                'ptid' => $playTransaction->id,
                'payment_provider' => 'midtrans',
                'payment_id' => $notification->transaction_id,
                'response_data' => json_encode($request->getContent()),
                'status' => $status,
            ]);
        }

        return response()->json(['status' => $statusText]);
    }
}
