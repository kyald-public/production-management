<?php

namespace App\Http\Controllers;


use App\Http\Requests\UserRequest\UserCreateRequest;
use App\Http\Requests\UserRequest\UserUpdateRequest;
use App\Http\Requests\UserRequest\UserDestroyRequest;
use App\Http\Requests\UserRequest\UserShowRequest;
use App\Http\Requests\UserRequest\UserUpdateTokenRequest;
use App\Interfaces\UserInterface;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Notifications\GeneralNotification;
use Illuminate\Support\Facades\Notification;

class UserController extends BaseController
{
    private $userService;
    private $userInterface;

    public function __construct(
        UserService $userService,
        UserInterface $userInterface
    ) {
        $this->userService = $userService;
        $this->userInterface = $userInterface;
    }

    public function index(UserShowRequest $request)
    {
        request()->request->add([
            'by_id' => auth('sanctum')->user()->id,
            'collection_type' => 'showBasic',
        ]);

        $columnRequest = array('*');

        $getUserList = $this->userInterface->index(request()->all(), $columnRequest);

        if ($getUserList['queryStatus']) {

            return $this->handleResponse($getUserList['queryResponse'], 'show user list success', $request->all(), str_replace('/', '.', $request->path()), 200);
        }

        return $getUserList;

    }

    public function show(UserShowRequest $request)
    {
        request()->request->add([
            'by_id' => auth('sanctum')->user()->id,
            'collection_type' => 'showBasic',
        ]);

        $columnRequest = array('*');
        $getUserList = $this->userInterface->show($request->all(), $columnRequest);

        if ($getUserList['queryStatus']) {

            return $this->handleResponse($getUserList['queryResponse'], 'show user detail success', $request->all(), str_replace('/', '.', $request->path()), 200);
        }

        return $getUserList;

    }

    public function create(UserCreateRequest $request)
    { //done

        $insert = $this->userInterface->store($request->all(), 'show_all');

        if ($insert['queryStatus']) {

            return $this->handleResponse($insert['queryResponse'], 'Insert user success', $request->all(), str_replace('/', '.', $request->path()), 201);
        } else {

            $data = array([
                'field' => 'create-user',
                'message' => 'user create fail',
            ]);

            return $this->handleError($data, $insert['queryMessage'], $request->all(), str_replace('/', '.', $request->path()), 422);
        }

    }

    public function update(UserUpdateRequest $request)
    { //done

        $update = $this->userInterface->update($request->all(), 'show_all');

        if ($update['queryStatus']) {

            $data = array(
                'field' => 'update-user',
                'message' => 'user successfully updated',
            );

            return $this->handleResponse($data, 'Update user success', $request->all(), str_replace('/', '.', $request->path()), 201);
        } else {

            $data = array([
                'field' => 'update-user',
                'message' => 'user update fail',
            ]);

            return $this->handleError($data, $update['queryMessage'], $request->all(), str_replace('/', '.', $request->path()), 422);
        }

    }

    public function destroy(UserDestroyRequest $request)
    {

        //remove data
        $destroy = $this->userInterface->destroy($request->by_uuid);

        if ($destroy['queryStatus']) {

            //response
            $data = array(
                'field' => 'destroy-user',
                'message' => 'user successfuly destroyed',
            );

            return $this->handleResponse($data, 'Destroy user  success', $request->all(), str_replace('/', '.', $request->path()), 204);

        } else {

            $data = array([
                'field' => 'destroy-user',
                'message' => 'user destroy fail',
            ]);

            return $this->handleError($data, $destroy['queryMessage'], $request->all(), str_replace('/', '.', $request->path()), 422);
        }

    }

    public function refreshToken(UserUpdateTokenRequest $request)
    { //done

        $update = $this->userInterface->refreshToken($request->all(), 'show_all');

        if ($update['queryStatus']) {

            $data = array(
                'field' => 'update-user',
                'message' => 'user token successfuly updated',
            );

            return $this->handleResponse($data, 'Update user success', $request->all(), str_replace('/', '.', $request->path()), 201);
        } else {

            $data = array([
                'field' => 'update-user',
                'message' => 'user update fail',
            ]);

            return $this->handleError($data, $update['queryMessage'], $request->all(), str_replace('/', '.', $request->path()), 422);
        }

    }



}
