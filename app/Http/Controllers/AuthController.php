<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthRequest\LoginUsersPinRequest;
use App\Http\Requests\AuthRequest\LoginUsersRequest;
use App\Http\Requests\AuthRequest\RegisterUsersRequest;
use App\Http\Requests\AuthRequest\SocialUsersRequest;
use App\Http\Requests\UserRequest\UserUpdateRequest;
use App\Interfaces\UserInterface;
use App\Services\AuthServices\LoginService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends BaseController
{
    private $LoginService;
    private $userInterfaces;

    public function __construct(
        LoginService $LoginService,
        UserInterface $userInterfaces
    ) {
        $this->LoginService = $LoginService;
        $this->userInterfaces = $userInterfaces;
    }

    /**
     * @lrd:start
     * # apabila login dari halaman ini, maka secara otomatis sistem akan menaro token dalam session sehingga tidak perlu dimasukan dalam bearer yang tertera  di atas,
     * dan sistem akan mengambil session yang paling terakhir, sehingga apabila ingin authnya berdasarkan token, maka dapatkan token/login dari platform lain seperti postman lalu taro tokennya di bearear di atas kanan
     *
     * @lrd:end
     */
    public function login(LoginUsersRequest $request)
    {

        if ($request->isEmail == false) {
            $loginUser = $this->LoginService->usernamePassword($request->only('username', 'password', 'device_id'));
        } else {
            $loginUser = $this->LoginService->emailPassword($request->only('email', 'password', 'device_id'));
        }

        if ($loginUser['arrayStatus']) {

            $reformatRequest = $request->all();
            $reformatRequest['password'] = '[protected]';

            return $this->handleResponse($loginUser['arrayResponse'], 'login success', $reformatRequest, str_replace('/', '.', $request->path()), 200);
        } else {
            $data = array([
                'field' => 'login',
                'message' => 'username/password is not correct',
            ]);
            return $this->handleError($data, $loginUser['arrayMessage'], $request->all(), str_replace('/', '.', $request->path()), 422);

        }
    }

    public function register(RegisterUsersRequest $request)
    {

        $loginUser = $this->LoginService->registerEmail($request->only('username', 'password'));

        if ($loginUser['arrayStatus']) {

            $reformatRequest = $request->all();
            $reformatRequest['password'] = '[protected]';

            return $this->handleResponse($loginUser['arrayResponse'], 'login success', $reformatRequest, str_replace('/', '.', $request->path()), 200);
        } else {
            $data = array([
                'field' => 'login',
                'message' => 'username/password is not correct',
            ]);
            return $this->handleError($data, $loginUser['arrayMessage'], $request->all(), str_replace('/', '.', $request->path()), 422);

        }
    }


    public function unlock(LoginUsersPinRequest $request)
    {
        $loginUser = $this->LoginService->emailPin($request->only('email', 'pin'));

        if ($loginUser['arrayStatus']) {

            $reformatRequest = $request->all();
            $reformatRequest['password'] = '[protected]';
            $reformatRequest['pin'] = '[protected]';

            return $this->handleResponse($loginUser['arrayResponse'], 'unlock success', $reformatRequest, str_replace('/', '.', $request->path()), 200);
        } else {
            $data = array([
                'field' => 'login',
                'message' => 'username/pin is not correct',
            ]);
            return $this->handleError($data, $loginUser['arrayMessage'], $request->all(), str_replace('/', '.', $request->path()), 422);

        }
    }

    public function social(SocialUsersRequest $request)
    {
        $authUser = $this->LoginService->sociaLogin($request);

        if ($authUser['arrayStatus']) {
            $reformatRequest = $request->all();
            return $this->handleResponse($authUser['arrayResponse'], 'login success', $reformatRequest, str_replace('/', '.', $request->path()), 200);

        } else {
            return $this->handleError(null, $authUser['arrayMessage'], $request->all(), str_replace('/', '.', $request->path()), 422);
        }

    }

    public function socialCheck(SocialUsersRequest $request)
    {
        $authUser = $this->LoginService->sociaLoginCheck($request);

        if ($authUser['arrayStatus']) {
            $reformatRequest = $request->all();
            return $this->handleResponse($authUser['arrayResponse'], 'login success', $reformatRequest, str_replace('/', '.', $request->path()), 200);

        } else {
            return $this->handleError(null, $authUser['arrayMessage'], $request->all(), str_replace('/', '.', $request->path()), 422);
        }

    }

    public function profile(Request $request)
    {
        request()->request->add([
            'by_id' => auth('sanctum')->user()->id,
            'collection_type' => 'showBasic',
        ]);

        $columnRequest = array('*');
        $getUserProfile = $this->userInterfaces->showProfile($request->all(), $columnRequest);

        if ($getUserProfile['queryStatus']) {

            return $this->handleResponse($getUserProfile['queryResponse'], 'show my profile users success', $request->all(), str_replace('/', '.', $request->path()), 200);
        }

        return $getUserProfile;

    }

    public function update(UserUpdateRequest $request)
    { //done

        $update = $this->userInterfaces->update(Auth::user()->id, $request->except(['uuid']), 'show_all');

        if ($update['queryStatus']) {

            $data = array(
                'field' => 'update-user',
                'message' => 'user successfuly updated',
            );

            return $this->handleResponse($data, 'Update user success', $request->all(), str_replace('/', '.', $request->path()), 201);
        } else {

            $data = array([
                'field' => 'update-user',
                'message' => 'user update fail',
            ]);

            return $this->handleError($data, $update['queryMessage'], $request->all(), str_replace('/', '.', $request->path()), 422);
        }

    }

    public function logout(Request $request)
    {

        auth('sanctum')->user()->currentAccessToken()->delete();

        $data = array(
            'field' => 'logout',
            'message' => 'logout account success',
        );

        return $this->handleResponse($data, 'logout users success', $request->all(), str_replace('/', '.', $request->path()), 200);

    }

}
