<?php

namespace App\Models;

use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Spatie\Permission\Traits\HasRoles;
use Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class City extends Model
{
    // if your key name is not 'id'
    // you can also set this to null if you don't have a primary key
    protected $primaryKey = 'city_id';

    use HasFactory, HasRoles, Notifiable, SoftDeletes;


    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults();
        // ->logOnly(['name', 'text']);
        // Chain fluent methods for configuration options
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
    ];

    protected $guard_name = 'sanctum';

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'deleted_at'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'created_at' => 'datetime',
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            if(empty($model->uuid)) {
                $model->uuid = Str::uuid();
            }
        });
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon\Carbon::parse($date)->format('Y-m-d H:i:s');

    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon\Carbon::parse($date)->format('Y-m-d H:i:s');

    }

}
