<?php

namespace App\Providers;

use App\Interfaces\UserInterface;
use App\Repositories\UsersRepository;

use Illuminate\Support\ServiceProvider;

use App\Interfaces\MiscInterface;
use App\Repositories\MiscRepository;

class RepositoriesServicesProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(UserInterface::class,UsersRepository::class);
        $this->app->bind(MiscInterface::class,MiscRepository::class);
    }
}
