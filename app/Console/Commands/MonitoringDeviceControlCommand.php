<?php

namespace App\Console\Commands;

use AWS;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class MonitoringDeviceControlCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mqtt:monitoring-device-control';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "it's only for testing, to automatically enabled and disabled device";
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
    ) {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $tables = IoTDevices::all(); // all available devices

        if (count($tables)) {
            foreach ($tables as $table) {

                    $topic = $table->mqttTopic();

                    $availability = $this->deviceAvailability($table, $topic);

                    $availability = json_decode(json_encode($availability), true);
                    $availability['diff_minutes'] = Carbon::now()->diffInMinutes(Carbon::createFromTimestamp($availability['message']['metadata']['reported']['status']['timestamp']));
                    $availability['is_online'] = $availability['diff_minutes'] < config('mqtt.minutes_device_offline_threshold');

                    $isNotify = true;

                    if (!$availability['is_online']) {
                        $availability['message'] = "Device is offline for " . $availability['diff_minutes'] . " Minutes";

                        $errors = [
                            "table" => $table->name,
                            "location" => $table->location->name,
                            "state" => "offline",
                            "last_notify_date" => Carbon::now()->format('Y-m-d H:i:s'),
                            "message" => $availability['message'],
                        ];

                        $this->log(json_encode($errors));

                        if ($table->notify_error_at) {
                            $isNotify = Carbon::now()->diffInMinutes($table->notify_error_at) > config('mqtt.minutes_device_offline_notification_gap');
                        }

                        if ($isNotify) {

                            $table->notify_error_at = Carbon::now()->format('Y-m-d H:i:s');
                            $table->save();


                            // Send email notification
                            $admins = Admin::where('status', 1)->get();
                            if (count($admins)) {
                                foreach ($admins as $admin) {
                                    $admin->notify((new DeviceMonitorNotification($table, $errors))->onConnection('database')->onQueue('monitoring')->delay(now()->addSeconds(config('app.notification_delay'))));
                                }
                            }
                        }

                    }

                    if ($availability['is_online'] && $table->notify_error_at) {

                        $availability['message'] = "Device " . $table->name . " is back online";

                        $table->notify_error_at = null;
                        $table->save();

                        $errors = [
                            "table" => $table->name,
                            "location" => $table->location->name,
                            "state" => "online",
                            "last_notify_date" => Carbon::now()->format('Y-m-d H:i:s'),
                            "message" => $availability['message'],
                        ];

                        $this->log(json_encode($errors));

                        // Send email notification
                        $admins = Admin::where('status', 1)->get();
                        if (count($admins)) {
                            foreach ($admins as $admin) {
                                $admin->notify((new DeviceMonitorNotification($table, $errors))->onConnection('database')->onQueue('monitoring')->delay(now()->addSeconds(config('app.notification_delay'))));
                            }
                        }

                    }
                }
        }

    }

    private function log($message)
    {
        Log::channel('device_monitor')->error($message);
    }

    private function logInfo($message)
    {
        Log::channel('device_monitor')->info($message);
    }

    public function deviceAvailability($table, $topic)
    {

        $originalTopic = $topic;
        try {

            $iotDataPlaneClient = AWS::createClient('IotDataPlane');
            $result = $iotDataPlaneClient->getThingShadow([
                'thingName' => $table->iot_things,
                'shadowName' => $table->iot_device_id,
            ]);

            $message = json_decode($result['payload']->getContents());

            $reported = $message->state->reported;
            $messageTopic = [
                'status' => $reported->status,
                'state' => $reported->state,
                'message' => $message,
            ];

        } catch (\Exception $e) {
            $result = 0;
            $messageTopic = [
                'status' => config('play_transaction.device_status.OFF'),
                'state' => config('play_transaction.device_state.IDLE'),
                'result' => $result,
            ];
        }

        return $messageTopic;
    }

}
