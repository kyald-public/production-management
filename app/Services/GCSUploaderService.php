<?php

namespace App\Services;

use App\Http\Controllers\BaseController;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use File;

/**
 * Class S3uploaderServices
 * @package App\Services
 */
class GCSUploaderService extends BaseController
{
    public function uploadgcsStorage(UploadedFile $file, $path, $pathCurrentFile = null)
    {
        if ($file) {

            if ($pathCurrentFile) {

                $this->removeFile($pathCurrentFile);
            }

            $file_name = $file->getClientOriginalName();
            $file_type = $file->getClientOriginalExtension();
            $file_name_store = rand(2, 1000) . '_' . time() . '_' . Str::replace(' ', '_', $file->getClientOriginalName());
            $filePath = '' . $path . '/' . $file_name_store;

            Storage::disk('gcs')->put($filePath, File::get($file));

            $file = [
                'fileNameOriginal' => $file_name,
                'fileNameStore' => $file_name_store,
                'fileType' => $file_type,
                'filePath' => $filePath,
                'fileUrl'  => getEnv('GOOGLE_CLOUD_STORAGE_PATH').$filePath,
                'fileSize' => $this->fileSize($file),
            ];

            return $this->handleArrayResponse($file, 'upload image success', 'info');
        }
    }
    public function removeFile($pathCurrentFile)
    {

        return Storage::disk('gcs')->delete($pathCurrentFile);

    }
    public function fileSize($file, $precision = 2)
    {
        $size = $file->getSize();

        if ($size > 0) {
            $size = (int) $size;
            $base = log($size) / log(1024);
            $suffixes = array(' bytes', ' KB', ' MB', ' GB', ' TB');
            return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
        }

        return $size;
    }
}
