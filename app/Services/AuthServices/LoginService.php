<?php

namespace App\Services\AuthServices;

use App\Http\Controllers\BaseController;
use App\Models\User;
use App\Models\UserSocialAccount;
use Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;

class LoginService extends BaseController
{

    public function emailPassword($datas)
    {

        if (Auth::attempt(['email' => $datas['email'], 'password' => $datas['password']])) {
            $auth = Auth::user();

            if(isset($datas['device_id'])){
                $auth->device_id = $datas['device_id'];
                $auth->save();
            }
            
            $success['token'] = $auth->createToken('loginAsUsers', ['api_access'])->plainTextToken;
            $success['name'] = $auth->name;
            $success['uuid'] = $auth->uuid;

            return $this->handleArrayResponse($success, 'success login users');
        } else {
            return $this->handleArrayErrorResponse('unauthenticate', 'service - email or password users not correct');
        }
    }
    public function usernamePassword($datas)
    {

        if (Auth::attempt(['username' => $datas['username'], 'password' => $datas['password']])) {
            $auth = Auth::user();
            

            if(isset($datas['device_id'])){
                $auth->device_id = $datas['device_id'];
                $auth->save();
            }

            $success['token'] = $auth->createToken('loginAsUsers', ['api_access'])->plainTextToken;
            $success['name'] = $auth->name;

            return $this->handleArrayResponse($success, 'success with username login users');
        } else {
            return $this->handleArrayErrorResponse('unauthenticate', 'service - username or password users not correct');
        }
    }

    public function registerEmail($datas)
    {

        $newUser['username'] = $datas['username'];
        $newUser['name'] = '';
        $newUser['email'] = $datas['username'];
        $newUser['avatar'] = '';
        $newUser['password'] = bcrypt($datas['password']);
        $newUser['pin'] = bcrypt($datas['password']);
        $newUser['email_verified_at'] = now();
        $newUser['remember_token'] = Str::random(10);
        $newUser['has_created_password'] = 1;

        $user = User::create($newUser);

        $user->assignRole('admin');

        Auth::login($user, false);

        if (Auth::user()) {
            $auth = Auth::user();
            $success['token'] = $auth->createToken('loginAsUsers', ['api_access'])->plainTextToken;
            $success['name'] = $auth->name;
            $success['uuid'] = $auth->uuid;

            return $this->handleArrayResponse($success, 'success register users');
        } else {
            return $this->handleArrayErrorResponse('unauthenticate', 'service - email or password users not correct');
        }
    }

    public function sociaLogin($request)
    {

        try {
            $authUser = Socialite::driver($request->provider)->stateless()->userFromToken($request->access_token);
        } catch (\Exception $e) {
            return $this->handleArrayErrorResponse('unauthenticate', 'Invalid credentials');
        }

        $user = $this->findUser($authUser, $request);

        Auth::login($user, false);

        if (Auth::user()) {
            $auth = Auth::user();

            $success['token'] = $auth->createToken('loginAsUsers', ['api_access'])->plainTextToken;
            $success['name'] = $auth->name;
            $success['verified_at'] = $auth->email_verified_at;
            // $success['is_guest']        =  $auth->email_verified_at;

            return $this->handleArrayResponse($success, 'success login users');
        } else {
            return $this->handleArrayErrorResponse('unauthenticate', 'service - email or password users not correct');
        }

    }

    public function sociaLoginCheck($request)
    {

        try {
            $authUser = Socialite::driver($request->provider)->stateless()->userFromToken($request->access_token);
        } catch (\Exception $e) {
            return $this->handleArrayErrorResponse('unauthenticate', 'Invalid credentials');
        }

        $account = UserSocialAccount::where('provider', $request->provider)
            ->where('provider_user_id', $authUser->getId())
            ->first();

        if ($account) {
            $user = User::where('id', $account->user_id)->first();
            Auth::login($user, false);
        }

        if (Auth::user()) {
            $auth = Auth::user();

            $success['token'] = $auth->createToken('loginAsUsers', ['api_access'])->plainTextToken;
            $success['name'] = $auth->name;
            $success['verified_at'] = $auth->email_verified_at;
            // $success['is_guest']        =  $auth->email_verified_at;

            return $this->handleArrayResponse($success, 'success login users');
        } else {
            return $this->handleArrayErrorResponse('unauthenticate', 'user not found');
        }

    }

    public function findUser($providerUser, $request)
    {

        $account = UserSocialAccount::where('provider', $request->provider)
            ->where('provider_user_id', $providerUser->getId())
            ->first();

        if ($account) {
            $user = User::where('id', $account->user_id)->first();
            return $user;
        } else {

            if ($request->provider == 'google') {
                $newUser['username'] = @$providerUser->email;
                $newUser['name'] = @$providerUser->name;
                $newUser['email'] = @$providerUser->email;
                $newUser['avatar'] = @$providerUser->avatar ??
                    "http://dashboard.perks.id/photo/asset/FB8YQ03uMfAZfrQXx5Ir1QozkJAAR2suftgsfOkB.png";
            }

            $tempPassword = Str::random(10);
            $newUser['password'] = bcrypt($tempPassword);
            $newUser['pin'] = bcrypt($tempPassword);
            $newUser['uuid'] = Str::uuid();
            $newUser['email_verified_at'] = now();
            $newUser['remember_token'] = Str::random(10);
            $newUser['has_created_password'] = 0;

            $user = User::create($newUser);

            $user->assignRole('admin');

            UserSocialAccount::create([
                'user_id' => $user->id,
                'provider_user_id' => $providerUser->getId(),
                'provider' => $request->provider,
                'data' => (array) $providerUser,
            ]);
            return $user;
        }
    }

}
