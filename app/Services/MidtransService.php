<?php
namespace App\Services;
use App\Exceptions\CustomException;
use Midtrans\Config;
use Midtrans\Snap;
use Carbon\Carbon;
use Illuminate\Support\Str;

class MidtransService
{
    public function __construct()
    {
        // Set Midtrans credentials
        Config::$serverKey = config('services.midtrans.server_key');
        Config::$isProduction = config('services.midtrans.is_production');
    }

    public function getSnapToken(Transaction $transaction)
    {
        $user = $transaction->user;

        try {
            $now = Carbon::now();
            $seconds = $now->diffInSeconds($transaction->expired_payment_at);

            $transactionDetails = [
                'transaction_details' => [
                    'currency' => 'IDR',
                    'order_id' => $transaction->id.'-'.$transaction->code.'-'.Str::random(5),
                    'gross_amount' => $transaction->price,
                ],
                'customer_details' => [
                    'first_name' => $user->fname,
                    'last_name' => $user->lname,
                    'email' => $user->email,
                    'phone' => $user->phone,
                ],
                'item_details' => [
                    [
                        'quantity' => 1,
                        'id' => $transaction->id,
                        'price' => $transaction->price,
                        'name' => $transaction->code,
                    ]
                ],
                'credit_card' => [
                    'secure' => false,
                    'authentication' => 'none',
                    'save_card' => true,
                ],
                'user_id' => $user->email.$user->uid,
                'expiry' => [
                    'start_time' => date('Y-m-d H:i:s') . ' +0700',
                    'unit' => 'seconds',
                    "duration" => $seconds,
                ],
                'callbacks' => [
                    'finish' => route('checkout.complete'), 
                    'unfinish' => route('checkout.unfinish'), 
                    'error' => route('checkout.error'),
                    // 'finish' => config('app.nuxt_url') . '/payment/success', 
                    // 'unfinish' => config('app.nuxt_url') . '/payment/unfinish', 
                    // 'error' => config('app.nuxt_url') . '/payment/error',
                ],
            ];

            $token = Snap::createTransaction($transactionDetails);
        } catch (\Exception $e) {
            throw new CustomException($e->getMessage());
        }

        return $token;
    }
}
