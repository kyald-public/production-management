<?php
namespace App\Notifications;

use App\Channels\FcmChannel;
use Illuminate\Notifications\Notification;

class GeneralNotification extends Notification
{
    var $title;
    var $body;
    var $event;

    public function __construct($data)
    {
        $this->title = $data['title'];
        $this->body = $data['body'];
        $this->event = $data['event'];
    }

    public function via($notifiable)
    {
        return [FcmChannel::class];
    }
}
