<?php

namespace App\PipelineFilters\UserPipeline;

use App\Models\User;
use App\Models\UserMerchant;
use Auth;
use Closure;

class UserQueryPipeline
{
    public $getUser;

    public function handle($params, Closure $next)
    {

        $getUser = User::query();

        $this->getByKey($params, $getUser);

        return $next($getUser);
    }
    private function getByKey($params, $getUser)
    {
        if (!empty($params['by_uuid']) && $params['by_uuid']) {
            $getUser->where('uuid', $params['by_uuid']);
        }
        if (!empty($params['by_user_id']) && $params['by_user_id']) {
            $getUser->where('id', $params['by_user_id']);
        }
        if (!empty($params['by_email']) && $params['by_email']) {
            $getUser->where('contact_email', $params['by_email']);
        }
        if (!empty($params['by_role']) && $params['by_role']) {
            $getUser->role($params['by_role']);
        }
        if (!empty($params['by_user']) && $params['by_user']) {
            $getUser->where('fk_user_id', $params['by_user']);
        }
        if (!empty($params['by_order_number']) && $params['by_order_number']) {
            $getUser->where('order_number', $params['by_order_number']);
        }

        $userData = Auth::user();

        if ($userData->merchant) {
            $userIds = UserMerchant::where('merchant_id', $userData->merchant->merchant_id)->pluck('user_id')->toArray();

            $getUser->whereIn('id', $userIds);
        } else {
            $getUser->whereIn('id', []);
        }

        return $getUser;
    }

}
