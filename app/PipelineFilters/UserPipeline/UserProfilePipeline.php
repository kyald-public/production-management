<?php

namespace App\PipelineFilters\UserPipeline;

use App\Models\User;
use App\Models\UserMerchant;
use Auth;
use Closure;

class UserProfilePipeline
{
    public $getUser;

    public function handle($params, Closure $next)
    {

        $getUser = User::query();

        $this->getByKey($params, $getUser);
        // $this->getByWord($params, $getUser);
        // $this->getBySort($params, $getUser);

        return $next($getUser);
    }
    private function getByKey($params, $getUser)
    {
        // if (!empty($params['by_uuid']) && $params['by_uuid']) {
        //     $getUser->where('uuid', $params['by_uuid']);
        // }
        // if (!empty($params['by_user_id']) && $params['by_user_id']) {
        //     $getUser->where('id', $params['by_user_id']);
        // }
        // if (!empty($params['by_email']) && $params['by_email']) {
        //     $getUser->where('contact_email', $params['by_email']);
        // }
        // if (!empty($params['by_role']) && $params['by_role']) {
        //     $getUser->role($params['by_role']);
        // }
        // if (!empty($params['by_user']) && $params['by_user']) {
        //     $getUser->where('fk_user_id', $params['by_user']);
        // }
        // if (!empty($params['by_order_number']) && $params['by_order_number']) {
        //     $getUser->where('order_number', $params['by_order_number']);
        // }

        $userData = Auth::user();
        $getUser->where('id', $userData['id']);

        // if ($userData->merchant) {
        //     $userIds = UserMerchant::where('merchant_id', $userData->merchant->merchant_id)->pluck('user_id')->toArray();

        //     $getUser->whereIn('id', $userIds);
        // } else {
        //     $getUser->whereIn('id', []);
        // }

        return $getUser;
    }

    private function getByWord($params, $getUser)
    {
        if (!empty($params['keyword'])) {
            $keyword = $params['keyword'];
            $getUser->where(function ($getUser) use ($keyword) {
                // $getUser->where('shipping_first_name', 'like', '%' . $keyword . '%');
                // $getUser->orWhere('shipping_last_name', 'like', '%' . $keyword . '%');
                // $getUser->orWhere('order_number', 'like', '%' . $keyword . '%');
                // $getUser->orWhere('contact_email', 'like', '%' . $keyword . '%');
            });
        }
        return $getUser;
    }

    private function getBySort($params, $getUser)
    {
        if (!empty($params['sort_by'])) {

            if (!empty($params['sort_type'])) {

                $sort_type = $params['sort_type'];
            } else {
                $sort_type = 'desc';
            }

            $getUser->orderBy('' . $params['sort_by'] . '', $sort_type);
        } else {

            $getUser->orderBy('created_at', 'desc');
        }

        return $getUser;
    }
}
