<?php

namespace App\PipelineFilters\MiscPipeline;

use App\Models\Province;
use App\Models\City;
use Closure;

class MiscQueryPipeline
{

    var $getCity;
    var $getProvince;

    public function handleCity($params, Closure $next)
    {

        $getCity = City::query();

        $this->getByKey($params, $getCity);
        $this->getByWord($params, $getCity);
        $this->getBySort($params, $getCity);

        $getCity->where('province_id', $params['province_id']);

        return $next($getCity);
    }


    public function handleProvince($params, Closure $next)
    {

        $getProvince = Province::query();

        $this->getByKey($params, $getProvince);
        $this->getByWord($params, $getProvince);
        $this->getBySort($params, $getProvince);

        return $next($getProvince);
    }

}
