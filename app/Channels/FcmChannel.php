<?php

namespace App\Channels;

use App\Models\Notification as NotificationModel;
use App\Notifications\GeneralNotification;
use Kreait\Firebase\Messaging\CloudMessage;

class FcmChannel
{
    public function send($notifiable, GeneralNotification $notification)
    {

        try {
            $deviceToken = $notifiable->device_id;

            $firebase = app('firebase.messaging');

            $message = CloudMessage::fromArray([
                'token' => $deviceToken,
                'notification' => [
                    "title" => $notification->title,
                    "body" => $notification->body,
                ],
                'data' => [
                    "event" => $notification->event,
                    "destination" => "",
                    "title" => $notification->title,
                    "body" => $notification->body,
                ],
            ]);

            $firebase->send($message);

            // Save the notification to the database

            $notification = NotificationModel::create([
                'type' => "notification",
                'notifiable_id' => $notifiable->id,
                'notifiable_type' => "App\Models\User",
                'data' => json_encode($notification),
            ]);
        } catch (\Exception $e) {
            $notification = NotificationModel::create([
                'type' => "notification",
                'notifiable_id' => $notifiable->id,
                'notifiable_type' => "App\Models\User",
                'data' => $e->getMessage(),
            ]);
        }

    }

}
