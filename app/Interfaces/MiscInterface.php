<?php

namespace App\Interfaces;

use Illuminate\Http\Request;

interface MiscInterface {
    public function indexCity($request,array $getOnlyColumn);
    public function indexProvince($request,array $getOnlyColumn);
}
