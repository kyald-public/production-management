<?php

namespace App\Interfaces;

use Illuminate\Http\Request;

interface UserInterface {
    public function index($request,array $getOnlyColumn);
    public function show($request,array $getOnlyColumn);
    public function showProfile($request,array $getOnlyColumn);
    public function store(array $data,$returnCollection);
    public function update($data,$returnCollection);
    public function refreshToken($data,$returnCollection);
    public function destroy($id);
}
